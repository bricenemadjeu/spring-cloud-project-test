package org.proxy.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableHystrix
public class GatewayConfig {

    @Autowired
    AuthenticationFilter filter;

    @Bean
    @LoadBalanced
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                // Add new company API
                .route("COMPANY-SERVICE",p -> p.path("/company/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://COMPANY-SERVICE"))

                // With product name as Param, get product information (PHP Web service)
                .route("service-product", p -> p.path("/testApiPHP/")
                        .uri("http://127.0.0.1:80/testApiPHP/"))


                // Get Users API
                .route("USERS-SERVICE",p -> p.path("/users/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://USERS-SERVICE"))

                .route("AUTHENTIFICATION-SERVICE",p -> p.path("/auth/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://AUTHENTIFICATION-SERVICE"))

                .build();
    }
}
