package og.company.demo.controller;

import og.company.demo.dao.CompanyRepository;
import og.company.demo.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @Value("${xParam}")
    private int xParam;

    @Value("${yParam}")
    private int yParam;

    @Value("${me}")
    private String me;

    @GetMapping("/myConfig")
    public Map<String, Object> myConfig(){
        Map<String, Object> param = new HashMap<>();
        param.put("xParam", xParam);
        param.put("yParam", yParam);
        param.put("me", me);
        return param;
    }

    @GetMapping("/allcompanies")
    public List<Company> findAllCompany(){
        return companyRepository.findAll();
    }


    @GetMapping("/company/{id}")
    public Optional<Company> getCompanyInfo(@PathVariable Long id){
        return companyRepository.findById(id);
    }

    @PostMapping("/addcompany")
    @Transactional
    public Map<String, Object> saveNewCompany(@RequestBody Company company){
        Map<String, Object> param = new HashMap<>();
        try {
            companyRepository.save(company);
            param.put("status", 200);
            param.put("message", "Successful save company");
            param.put("data", company);

            return param;
        } catch (Exception e){
            param.put("status", 500);
            param.put("message", e.getMessage());
            param.put("data", null);
            return param;
        }

    }
}
