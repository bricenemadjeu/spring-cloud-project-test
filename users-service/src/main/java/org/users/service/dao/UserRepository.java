package org.users.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.users.service.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
