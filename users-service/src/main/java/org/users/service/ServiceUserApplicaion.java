package org.users.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.users.service.dao.UserRepository;
import org.users.service.model.User;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceUserApplicaion {
    public static void main(String[] args) {
        SpringApplication.run(ServiceUserApplicaion.class, args);
    }


    @Bean
    CommandLineRunner initUsersData(UserRepository userRepository){
        return args -> {
            List<User> allusers = Arrays.asList(
                    new User(1000L, "admin", BCrypt.hashpw("admin", BCrypt.gensalt()) ,"ADMIN"),
                    new User(1001L, "brice", BCrypt.hashpw("brice", BCrypt.gensalt()) ,"MANAGER"),
                    new User(1002L, "vanel", BCrypt.hashpw("vanel", BCrypt.gensalt()), "SIMPLE_USER")
            );
            userRepository.saveAll(allusers);
        };

    }
}
