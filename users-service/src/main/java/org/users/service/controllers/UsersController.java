package org.users.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.users.service.dto.UserAuth;
import org.users.service.dto.UserDTO;
import org.users.service.model.User;
import org.users.service.services.UserServiceInterface;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserServiceInterface userServiceInterface;

    @GetMapping("/getUser/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable Long id){
        return ResponseEntity.ok(userServiceInterface.getUserById(id));
    }


    @PostMapping("/getUserByUserName")
    public UserDTO findUserByUsername(@RequestBody UserAuth userAuth){
        return userServiceInterface.getUserByUsernameAndPwd(userAuth);
    }


    @GetMapping("/getAllUsers")
    public List<UserDTO> findAllUsers(){
        return userServiceInterface.getAllUsers();
    }

    @PostMapping("/save-new-user")
    public ResponseEntity<UserDTO> registerUser(@RequestBody User user){
        return ResponseEntity.ok(userServiceInterface.saveUser(user));
    }

}
