package org.users.service.services;

import org.users.service.dto.UserAuth;
import org.users.service.dto.UserDTO;
import org.users.service.model.User;

import java.util.List;

public interface UserServiceInterface {

    UserDTO getUserById(Long id);

    UserDTO getUserByUsernameAndPwd(UserAuth userAuth);

    List<UserDTO> getAllUsers();

    UserDTO saveUser(User user);

    List<UserDTO> saveAllUser(List<User> users);
}
