package org.users.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.users.service.dao.UserRepository;
import org.users.service.dto.UserAuth;
import org.users.service.dto.UserDTO;
import org.users.service.model.User;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersSreviceImplement implements UserServiceInterface{

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO getUserById(Long id) {
        User user = userRepository.findById(id).get();
        return new UserDTO(user.getId(), user.getUsername(), user.getRole());
    }

    @Override
    public UserDTO getUserByUsernameAndPwd(UserAuth auth) {
        User user = userRepository.findByUsername(auth.getUsername());
        if(user!=null){
            if(BCrypt.checkpw(auth.getPassword(), user.getPassword()))
                return new UserDTO(user.getId(), user.getUsername(), user.getRole());
        }

        return null;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll().stream()
                .map(u -> new UserDTO(u.getId(), u.getUsername(), u.getRole())).collect(Collectors.toList());
    }

    @Override
    public UserDTO saveUser(User user) {
        userRepository.save(user);
        return new UserDTO(user.getId(), user.getUsername(), user.getRole());
    }

    @Override
    public List<UserDTO> saveAllUser(List<User> users) {
        return userRepository.saveAll(users).stream()
                .map(u -> new UserDTO(u.getId(), u.getUsername(), u.getRole())).collect(Collectors.toList());
    }
}
