package org.authentification.service.services;

import org.authentification.service.dto.UserDTO;
import org.authentification.service.model.JwtResponse;
import org.authentification.service.model.UserAuth;
import org.authentification.service.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthentificationService {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private final RestTemplate restTemplate;

    @Autowired
    public AuthentificationService(RestTemplate restTemplate, final JwtTokenUtil jwtTokenUtil) {
        this.restTemplate = restTemplate;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public ResponseEntity<?> createAuthenticationToken(UserAuth user) {
        String token = "";
        try {

            UserDTO userAut = restTemplate.postForObject("http://127.0.0.1:8080/getUserByUserName", user, UserDTO.class);
            Assert.notNull(userAut, "Authentification failed");

            token = jwtTokenUtil.generateToken(user, userAut.getRole(), userAut.getId().toString());

            return ResponseEntity.ok(new JwtResponse(token, "Authentification succed"));
        } catch(Exception e){
            e.printStackTrace();
            return  ResponseEntity.ok(new JwtResponse("", "Authentification failed"));
        }
    }
}
